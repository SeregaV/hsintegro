var groupArray = require('group-array');
var api = require('./api').default;
var auth = require('./auth.json');
var fs = require('fs');

var tokenFile = '/tmp/hsintegro-hubtoken.json';
var token = {};

async function setup() {
  if (!fs.existsSync(tokenFile)) {
    console.log('File not found!')
    await signin().catch(function (err) {
      return console.error(err);
    })
  }else{
    token = JSON.parse(new String(fs.readFileSync(tokenFile)));
  }
}


async function signin() {
  console.log(process.env.EMAIL);
  var authTokenStruct = await api({
    method: 'auth',
    apptoken: auth.apptoken,
    email: process.env.EMAIL,
    password: process.env.PASSWORD
  });

  if (authTokenStruct.error) {
    console.error(authTokenStruct.error);
  } else {
    fs.writeFileSync(tokenFile, JSON.stringify(authTokenStruct));
    token = authTokenStruct;
  }
}

// async function getActivities(startTime, endTime) {
//   var xactivities, activities;
//   await setup();
//   xactivities = await api({
//     method: 'activities',
//     apptoken: auth.apptoken,
//     authtoken: token.user.auth_token,
//     endTime: JSON.stringify(endTime),
//     startTime: JSON.stringify(startTime)
//   });
  
//   activities = groupArray(xactivities.activities, 'project_id') || [];
//   return activities
// }

async function getActivities(startTime, endTime) {
  let xactivities, activities, tempactivities = [];
  let offset = 0;
  await setup();

  do {
    console.log(startTime, endTime)
    xactivities = await api({
      method: 'activities',
      apptoken: auth.apptoken,
      authtoken: token.user.auth_token,
      endTime: JSON.stringify(endTime),
      startTime: JSON.stringify(startTime),
      offset: offset
    });
    console.log('got')
    tempactivities = [ ...tempactivities, ...xactivities.activities ]
    console.log('temps')
    offset += 500
    console.log(tempactivities.length, xactivities.activities.length)
  } while(xactivities.activities.length === 500)
  
  activities = groupArray(tempactivities, 'project_id') || [];
  return activities
}

async function getProjects() {
  var projects;
  await setup();
  projects = await api({
    method: 'projects',
    apptoken: auth.apptoken,
    authtoken: token.user.auth_token
  });
  return projects.projects || [];
}

async function getUsers() {
  var users;
  await setup();
  users = await api({
    method: 'users',
    apptoken: auth.apptoken,
    authtoken: token.user.auth_token
  });

  return users.users || [];
}

exports.signin = signin;
exports.getActivities = getActivities;
exports.getProjects = getProjects;
exports.getUsers = getUsers;