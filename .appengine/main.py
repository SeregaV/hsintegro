import webapp2
import urllib2
import urllib
import json
from google.appengine.api import urlfetch

urlfetch.set_default_fetch_deadline(180)

url = 'https://us-central1-ra-sandbox.cloudfunctions.net/hsintegro'
headers = {"cronrequest" : "true", 'Content-Type': 'application/json' }

archive = {
    'Trusty-Care': '1K94FhIUYg5IkYfoXtpmuoc93TKSwyA0pKZ6rKHDAp4A', 
    "GCP": "10H6S0u-zAKPsjknM6aijD6uzXlItv2sZOe5xJiTGfMg"
}

fst = { 
    'LMV': '1eGQY6J1nQZ5C7EJ9jIrw_7CpdtGHiW1XDUjPjmGj_I0',
    "VXP": "1TTb5OVjGJVleKYfFlUmWcTWhm45dQgiMROkQrkqs_o4"
}
sec = {
    "remoteassembly / ee-biomauris": "115VUuE_D8NwdLzlf0OFGPskF1215LQj8ae1kBff_Tlo",
    "remoteassembly/wedy": "1yXQ-dAoSFT_KB3KXCqvwXir6BSFiOXfyFLqDmljqyT0" 
}
thd = {
    "remoteassembly / pl": "1F0T_ob6l4LnqkNJ7ZWkRc34NTRSivO4dNCZQrn8f15Y",
    "remoteassembly / symplete": "1ZRxNBMe1mvFADUmEHZqdNQ-wWe5TGaHa49yen63Znzg"
}

class HourCronPage(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(120)
        response = urlfetch.fetch(url=url, method=urlfetch.POST, deadline=180, payload=json.dumps({ "sheets": fst }), headers=headers)
        self.response.write(response)
        # r = requests.post(url, data=fst)
        # request = urllib2.Request(url, headers)
        # contents = urllib2.urlopen(request).read()

class Hour2CronPage(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(120)
        response = urlfetch.fetch(url=url, method=urlfetch.POST, deadline=180, payload=json.dumps({ "sheets": sec }), headers=headers)
        self.response.write(response)
        # r = requests.post(url, data=sec)
        # request = urllib2.Request(url, headers)
        # contents = urllib2.urlopen(request).read()

class Hour3CronPage(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(120)
        response = urlfetch.fetch(url=url, method=urlfetch.POST, deadline=180, payload=json.dumps({ "sheets": thd }), headers=headers)
        self.response.write(response)
        # r = requests.post(url, data=thd)
        # request = urllib2.Request(url, headers)
        # contents = urllib2.urlopen(request).read()

app = webapp2.WSGIApplication([
    ('/hour/first', HourCronPage),
    ('/hour/second', Hour2CronPage),
    ('/hour/third', Hour3CronPage),
    ], debug=True)