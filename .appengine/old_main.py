import webapp2
import urllib2
from google.appengine.api import urlfetch

urlfetch.set_default_fetch_deadline(180)

class HourlyCronPage(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(55)
        response = urlfetch.fetch(url='https://us-central1-ra-sandbox.cloudfunctions.net/hsintegro', method=urlfetch.GET, deadline=180)
        self.response.write(response)

app = webapp2.WSGIApplication([
    ('/daily', HourlyCronPage),
    ('/daily-confirm', HourlyCronPage),
], debug=True)