const config = require ('./config').default;
const fetch = require('node-fetch');
const queryString = require ('query-string');

async function api (payload) {
  let method, meth, headers, xurl, xparams, response;
  method = payload.method;
  meth = config.method(payload)[method];
  headers = meth.headers;
  xurl = '' + config.apiURL + method;
  xparams = {
    method: meth.method,
    headers: Object.assign({'Content-Type': 'application/json'}, headers)
  };

  if (meth.method == 'POST') {
    xparams = Object.assign(xparams, {body: JSON.stringify(meth.params)});
  } else {
    xurl = xurl.concat('?' + queryString.stringify(meth.params));
  }

  console.info('Executing [' + method + ']...');
  
  response = await fetch(xurl, xparams).catch(function (err) {
    console.error(err);
  });

  return response.json();

};

exports.default = api