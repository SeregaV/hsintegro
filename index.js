const moment = require('moment');
const hubstaff = require ('./hubstaff');
const spreadssheets = require ('./spreadssheets').default;
const helpers = require ('./helpers');
const auth = require('./auth.json');
require('babel-polyfill');


async function forWeek() {
  var mon, fri, start, end, newDate;
  mon = moment().subtract(5, "days");
  fri = moment();
  start = new Date(mon.toDate().setHours(0, 0, 0, 0));
  end = new Date(fri.toDate().setHours(0, 0, 0, 0));

  while((start < end)) {
    newDate = new Date(moment(start).add(1, "days").toDate().setHours(0, 0, 0, 0));
    console.log('>> for: [date] ' + start.toLocaleDateString());
    await run(start, new Date(newDate), auth.sheets);
    start = new Date(newDate);
  }
}

async function run(startTime, endTime, sheets) {
  var activities, projects, users, bind_bind_users_projects;
  activities = await hubstaff.getActivities(startTime, endTime)
  projects = await hubstaff.getProjects();
  users = await hubstaff.getUsers();
  bind_bind_users_projects = await helpers.reorderProjectActivities(activities, projects, users);
  for(const sheet in sheets){
    await spreadssheets(bind_bind_users_projects, startTime, {[sheet]: sheets[sheet]});
  }
  console.info('Successful complete!');
}

// forWeek()

async function execute(sheets) {
  var startTime, endTime;
  startTime = new Date(moment().subtract(1, "days").toDate().setHours(0, 0, 0, 0));
  endTime = new Date(new Date().setHours(0, 0, 0, 0));

  console.log(startTime, endTime);
  return await run(startTime, endTime, sheets);
}

async function hsintegro(req, res) {
  const sheets = req.body.sheets || auth.sheets
  console.log(req.body)
  console.log(sheets)
  return await execute(sheets).then(function (resp) {
    res.send('Sucessful import!');
  }).catch(function (err) {
    res.send(err);
    console.error(err);
  });
};

// hsintegro(null, {send: (smth) => {console.log(smth)}})
exports.hsintegro = hsintegro;