var googleSpreadsheet = require('google-spreadsheet');
var credentials = require('./credentials.json');
var auth = require('./auth.json');
var bluebird = require('bluebird');
var moment = require('moment');
var _helpers = require('./helpers');
var titles = require('./titles').default;

async function fillZeros(cells, rowcount, colcount) {
  console.info('>> Filling zeros...');
  // снова получаем первый чистый
  let fstEmptyRowIndex = await _helpers.getFirstEmptyRowIndex(cells, rowcount, colcount);

  // если нету - берем последнюю не пустую строку
  if (!fstEmptyRowIndex) {
    fstEmptyRowIndex = rowcount + 1; // last non-empty row
    console.log('ROW COUNT');
  } else {
    // иначе форматируем в строку
    fstEmptyRowIndex /= colcount;
    fstEmptyRowIndex += 1;
    console.log('CALC ROW COUNT');
  }
  console.info({ fstEmptyRowIndex: fstEmptyRowIndex });

  // прходимся по первой строке
  for (let cell of cells.filter(c => c.row === 1)) {
    // если это имя
    if (cell.value !== 'Day' && cell.value) {
      // выделяем столбец
      // console.log(cell.value)
      var ucolumn = cells.filter(ucell => ucell.col === cell.col && !ucell.value && ucell.row < fstEmptyRowIndex); // Last non-empty row index

      // проходимся по столбцу и проставляем нули
      for (let ucell of ucolumn) {
        // console.log(ucell.value, ', ')
        if (!ucell.value) ucell.value = '0';
      }
    }
  }
}

async function accessSpreadsheet(projects, startTime, authsheets) {
  console.info('>> ACCESS SPREADSHEET');

  let sheets = [];
  for (let proj in authsheets) {
    var doc = bluebird.promisifyAll(new googleSpreadsheet(authsheets[proj]));
    await doc.useServiceAccountAuthAsync(credentials);
    sheets.push({ proj, doc });
  }

  for (let projec in authsheets) {
    var doc = sheets.find(sheeet => sheeet.proj === projec).doc; //bluebird.promisifyAll(new googleSpreadsheet(SHIT_ID));

    let info = await doc.getInfoAsync();
    console.info('>> Loaded doc: ' + info.title + ' by ' + info.author.email);
    for (let sheet of info.worksheets) {
      console.info('>> sheet 1: ' + sheet.title + ' ' + sheet.rowCount + 'x' + sheet.colCount);
    }

    if (!info.worksheets.some(shit => shit.title === 'APP')) {
      console.info('>> There is no worksheet for this project, creating new one...');
      await doc.addWorksheetAsync({ title: 'APP' /* rowCount: 1000, colCount: 26*/ });
    }

    let shit = info.worksheets.find(shit => shit.title === 'APP');
    let asyncShit = bluebird.promisifyAll(shit);

    let colcount = shit.colCount,
        rowcount = shit.rowCount;

    console.info('>> fetching cells...');

    let cells = await doc.getCellsAsync(shit.id, {
      'return-empty': true
    });

    cells[0].value = 'Day';

    let firstEmptyRowIndex = -1;

    // индекс ячейки даты
    let dateIndex = -1;

    // если есть текущая дата
    if (cells.some(cell => cell.value === moment(startTime).format('MM/DD/YYYY'))) {
      let cell = cells.filter(cell => cell.value === moment(startTime).format('MM/DD/YYYY'))[0];
      // выбрать ее
      dateIndex = cells.indexOf(cell);
    } else {
      // иначе найти следующую пустую строку
      firstEmptyRowIndex = _helpers.getFirstEmptyRowIndex(cells, rowcount, colcount);
      if (!firstEmptyRowIndex) {
        console.warn('There\'s no empty rows, need one more');
        let row = {
          'Day': moment(startTime).format('MM/DD/YYYY')
        };
        console.info(row);
        await doc.addRowAsync(shit.id, row);
        cells = await doc.getCellsAsync(shit.id, {
          'return-empty': true
        });
        rowcount++;

        let cell = cells.filter(cell => cell.value === moment(startTime).format('MM/DD/YYYY'))[0];
        // выбрать ее
        dateIndex = cells.indexOf(cell);
      } else {
        dateIndex = firstEmptyRowIndex;
      }

      // dateIndex = firstEmptyRowIndex //await _helpers.getFirstEmptyRowIndex(cells, rowcount, colcount)
    }
    console.log(dateIndex);

    // индекс ячейки с часами
    let hourCell = dateIndex + 1;

    // ставим дату и время
    cells[dateIndex].value = moment(startTime).format('MM/DD/YYYY');
    cells[hourCell].value = 'hour';

    for (const proj in projects) {

      let project = projects[proj];
      if (projec !== project.project) continue;

      SHIT_ID = authsheets[project.project];
      console.info('>> for:\t [project] ' + project.project + ' (SHIT_ID: ' + SHIT_ID + ')');

      if (!SHIT_ID) continue;

      // var doc = sheets.find(sheeet => sheeet.proj === project.project).doc //bluebird.promisifyAll(new googleSpreadsheet(SHIT_ID));

      // let info = await doc.getInfoAsync()

      // let shit = info.worksheets.find( shit => shit.title === 'APP' )
      // let asyncShit = bluebird.promisifyAll(shit)

      let users = [];
      for (userid in project.users) {
        users.push(project.users[userid].name);
      }

      // let cells = await doc.getCellsAsync(shit.id, {
      //   'return-empty': true
      // })

      let cell;
      // инжекс юзера в списке по порядку
      for (let userindex in users) {

        console.info('>> for:\t\t [user] ' + users[userindex]);

        // берем ячейку с именем пользователя или из первой строки и +3
        cell = cells.filter(cell => cell.value === users[userindex])[0] || cells.filter(cell => cell.row === 1 && cell.col === +userindex + 3)[0];

        // для незанятого пространства
        let emptyIndex;

        // если не нашло пустую ячейку или ячейку с именем пользователя
        if (!(cell && (!cell.value || cell.value === users[userindex]))) {
          // Найти слелующую пустую
          emptyIndex = await _helpers.getNextEmptyCellIndex(cells, 0, 3, colcount);
          console.info('>> next empty for [' + users[userindex] + '] is [' + emptyIndex + ']');

          // Записать имя
          cells[emptyIndex].value = users[userindex];
        } else {
          // Если нашло то просто записываем
          cell.value = users[userindex];
        }
      }

      // перебираем юзеров еще раз (нах?)
      for (let userid in project.users) {
        // каждый юзер
        let user = project.users[userid];

        const userindex = users.indexOf(user.name);
        const column = cells.indexOf(cells.filter(xcc => xcc.value === user.name)[0]);

        // получаем массив секунд
        let time = user.activities.map(function (act) {
          return act.tracked;
        });
        // console.log(time)
        // суммируем время
        let sumtime = time.reduce(function (a, b) {
          return a + b;
        }, 0);

        // форматируем время
        let trackedTime = (sumtime / 3600).toFixed(1).toString(); //.replace(".", ",");

        // вычисляем позици
        const userTimeCellIndex = eval(`${hourCell} + ${column} - 1`);
        //console.log(`${hourCell} + ${column} - 1 = ` + userTimeCellIndex)
        //console.log(userTimeCellIndex, cells[userTimeCellIndex].value, trackedTime)
        // вставляем время
        cells[userTimeCellIndex].value = trackedTime;
      }
    }
    await fillZeros(cells, rowcount, colcount);
    console.log('UPDATE');
    // обновляем ячейки
    // console.log(shit.bulkUpdateCellsAsync)
    const result = await asyncShit.bulkUpdateCellsAsync(cells);
    // console.log(result)
    console.log('DONE');
  }
}

exports.default = accessSpreadsheet;