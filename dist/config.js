exports.default = {
  apiURL: 'https://api.hubstaff.com/v1/',
  method: function method({ authtoken, apptoken, email, password, startTime, endTime, projects, taskId, projectId, offset }) {
    return {
      auth: {
        path: 'auth',
        method: 'POST',
        params: {
          email: email,
          password: password
        },
        headers: {
          'App-Token': apptoken,
          'Auth-Token': authtoken
        }
      },
      activities: {
        path: 'activities',
        method: 'GET',
        params: {
          'start_time': startTime,
          'stop_time': endTime,
          'offset': offset
        },
        headers: {
          'App-Token': apptoken,
          'Auth-Token': authtoken
        }
      },
      tasks: {
        path: 'tasks',
        method: 'GET',
        params: {
          'projects': projects && Array.isArray(projects) ? projects.join(',') : ''
        },
        headers: {
          'App-Token': apptoken,
          'Auth-Token': authtoken
        }
      },
      task: {
        path: `tasks/${taskId}`,
        method: 'GET',
        headers: {
          'App-Token': apptoken,
          'Auth-Token': authtoken
        }
      },
      projects: {
        path: 'projects',
        method: 'GET',
        params: {},
        headers: {
          'App-Token': apptoken,
          'Auth-Token': authtoken
        }
      },
      project: {
        path: 'project',
        method: 'GET',
        params: {
          'id': projectId
        },
        headers: {
          'App-Token': apptoken,
          'Auth-Token': authtoken
        }
      },
      users: {
        path: 'users?organization_memberships=true&project_memberships=true',
        method: 'GET',
        params: {},
        headers: {
          'App-Token': apptoken,
          'Auth-Token': authtoken
        }
      }
    };
  }
};