'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    'remoteassembly / ee-biomauris': 'BM',
    'remoteassembly / pl': 'PL',
    'Trusty-Care': 'TC',
    'GCP': 'GCP'
};