function toConsumableArray(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }return arr2;
  } else {
    return Array.from(arr);
  }
}

function getFirstEmptyRowIndex(cells, rows, columns) {
  for (let row = 1; row < rows; row++) {
    if (!cells[row * columns].value) return row * columns;
  }
  return null;
}

function getNextEmptyCellIndex(cells, row, column, columns) {
  var j;
  return regeneratorRuntime.async(function getNextEmptyCellIndex$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          j = column;

        case 1:
          if (!(j < columns)) {
            _context2.next = 7;
            break;
          }

          if (cells[row * columns + j].value) {
            _context2.next = 4;
            break;
          }

          return _context2.abrupt("return", row * columns + j);

        case 4:
          j++;
          _context2.next = 1;
          break;

        case 7:
          return _context2.abrupt("return", null);

        case 8:
        case "end":
          return _context2.stop();
      }
    }
  }, null, this);
}

function reorderProjectActivities(activities, projects, users) {
  let bind_acts_projs, _loop, act, bind_bind_users_projects, _loop2, proj;

  bind_acts_projs = {};

  _loop = function _loop(act) {
    var project = projects.find(function (project) {
      return project.id == act;
    }).name;
    bind_acts_projs[act] = {
      project: project,
      activities: [].concat(toConsumableArray(activities[act]))
    };
  };

  for (act in activities) {
    _loop(act);
  }

  bind_bind_users_projects = {};

  _loop2 = function _loop2(proj) {
    var project = bind_acts_projs[proj];
    project.users = {};

    var _loop3 = function _loop3(act) {
      var user = users.find(function (user) {
        return user.id == bind_acts_projs[proj].activities[act].user_id;
      });
      if (!project.users[user.id]) {
        project.users[user.id] = Object.assign(user, { activities: [] });
      }
      project.users[user.id].activities = project.users[user.id].activities.concat(bind_acts_projs[proj].activities[act]);
    };

    for (var act in bind_acts_projs[proj].activities) {
      _loop3(act);
    }
    delete project.activities;
    bind_bind_users_projects[proj] = project;
  };

  for (proj in bind_acts_projs) {
    _loop2(proj);
  }

  return bind_bind_users_projects;
}

exports.getFirstEmptyRowIndex = getFirstEmptyRowIndex;
exports.getNextEmptyCellIndex = getNextEmptyCellIndex;
exports.reorderProjectActivities = reorderProjectActivities;